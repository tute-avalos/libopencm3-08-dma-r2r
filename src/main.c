/**
 * @file main.c
 * @author Matías S. Ávalos (msavalos@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2021-01-13
 * 
 * @copyright Copyright (c) 2021
 * 
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/cm3/nvic.h>

#define GPIO0_TO_7 0x00FF
#define GPIO8_TO_15 0xFF00

const uint8_t sineLUT[] = {
    0x80, 0x85, 0x8B, 0x90, 0x96, 0x9B, 0xA0, 0xA6,
    0xAB, 0xB0, 0xB5, 0xBA, 0xBF, 0xC4, 0xC9, 0xCD,
    0xD1, 0xD6, 0xDA, 0xDE, 0xE1, 0xE5, 0xE8, 0xEB,
    0xEE, 0xF1, 0xF3, 0xF5, 0xF7, 0xF9, 0xFB, 0xFC,
    0xFD, 0xFE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFE,
    0xFD, 0xFC, 0xFB, 0xF9, 0xF7, 0xF5, 0xF3, 0xF1,
    0xEE, 0xEB, 0xE8, 0xE5, 0xE1, 0xDE, 0xDA, 0xD6,
    0xD1, 0xCD, 0xC9, 0xC4, 0xBF, 0xBA, 0xB5, 0xB0,
    0xAB, 0xA6, 0xA0, 0x9B, 0x96, 0x90, 0x8B, 0x85,
    0x80, 0x7A, 0x74, 0x6F, 0x69, 0x64, 0x5F, 0x59,
    0x54, 0x4F, 0x4A, 0x45, 0x40, 0x3B, 0x36, 0x32,
    0x2E, 0x29, 0x25, 0x21, 0x1E, 0x1A, 0x17, 0x14,
    0x11, 0x0E, 0x0C, 0x0A, 0x08, 0x06, 0x04, 0x03,
    0x02, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
    0x02, 0x03, 0x04, 0x06, 0x08, 0x0A, 0x0C, 0x0E,
    0x11, 0x14, 0x17, 0x1A, 0x1E, 0x21, 0x25, 0x29,
    0x2E, 0x32, 0x36, 0x3B, 0x40, 0x45, 0x4A, 0x4F,
    0x54, 0x59, 0x5F, 0x64, 0x69, 0x6F, 0x74, 0x7A
};

int main(void)
{
    rcc_clock_setup_in_hse_8mhz_out_72mhz();
    
    // Salida para el DAC R2R de 8 bits del PA0-PA7
    rcc_periph_clock_enable(RCC_GPIOA);
    gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO0_TO_7);

    // Toggle del PC13 como trigger (no es necesario para el funcionamiento del DMA)
    rcc_periph_clock_enable(RCC_GPIOC);
    gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);
    gpio_set(GPIOC, GPIO13);

    // Se habilita el clock del periferico DMA1
    rcc_periph_clock_enable(RCC_DMA1);
    
    // Se indica que es de MEM -> Periférico
    dma_set_read_from_memory(DMA1, DMA_CHANNEL7);
    // Origen de los datos a transferir:
    dma_set_memory_address(DMA1, DMA_CHANNEL7, (uint32_t)sineLUT);
    // Dirección de datos destino. El ODR (Output Data Register) del GPIOA:
    dma_set_peripheral_address(DMA1, DMA_CHANNEL7, (uint32_t)&GPIO_ODR(GPIOA));
    // Tamaño del dato a leer:
    dma_set_memory_size(DMA1, DMA_CHANNEL7, DMA_CCR_MSIZE_8BIT);
    // Tamaño del dato a escribir:
    dma_set_peripheral_size(DMA1, DMA_CHANNEL7, DMA_CCR_PSIZE_8BIT);
    // Cantidad de datos a transferir:
    dma_set_number_of_data(DMA1, DMA_CHANNEL7, 144);
    // Se incrementa automaticamente la posición en memoria:
    dma_enable_memory_increment_mode(DMA1, DMA_CHANNEL7);
    // La dirección destino se mantiene fija:
    dma_disable_peripheral_increment_mode(DMA1, DMA_CHANNEL7);
    // Se habilita la función de buffer circular:
    dma_enable_circular_mode(DMA1, DMA_CHANNEL7);
    // Se establece la prioridad del canal 7 del DMA1 como alta:
    dma_set_priority(DMA1, DMA_CHANNEL7, DMA_CCR_PL_VERY_HIGH);

    // Se habilita la interrupción que se ejecutan al finalizar
    // la transferencia para togglear un pin (no es necesario)
    dma_enable_transfer_complete_interrupt(DMA1, DMA_CHANNEL7);

    // Habilitación del canal:
    dma_enable_channel(DMA1, DMA_CHANNEL7);

    // Se habilita el clock del periférico del TIM4
    rcc_periph_clock_enable(RCC_TIM4);

    // Se configura el modo de funcionamiento del Timer4
    timer_set_mode(
        TIM4,               // Timer4
        TIM_CR1_CKD_CK_INT, // fuente Clk interno
        TIM_CR1_CMS_EDGE,   // Alineado por flanco
        TIM_CR1_DIR_UP);    // Cuenta ascendente

    timer_set_prescaler(TIM4, 0); // 72MHz / 1 => 72MHz

    // Se setea el valor hasta donde se cuenta:
    timer_set_period(TIM4, 49); // 72MHz / 50 = 1.44MHz => 1.44M/144 = 10kHz

    // Se habilita el evento del DMA en el overflow
    timer_set_dma_on_update_event(TIM4); // DMA1 channel7
    timer_enable_irq(TIM4, TIM_DIER_UDE);

    // Empieza a contar el Timer4
    timer_enable_counter(TIM4);

    // Se habilita en el NVIC la interrupción del DMA1-CH7
    nvic_enable_irq(NVIC_DMA1_CHANNEL7_IRQ);

    while (true) 
        ; // NADA el DMA + Timer4 manejan todo
}

/**
 * @brief Solo está para generar un pulso de "trigger"
 * 
 */
void dma1_channel7_isr(void)
{
    // Se bajan los flags:
    dma_clear_interrupt_flags(DMA1, DMA_CHANNEL7, DMA_FLAGS);
    // Se togglea el pin (LED)
    gpio_toggle(GPIOC, GPIO13);
}
